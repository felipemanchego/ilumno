import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { BackendService } from 'src/app/services/backend.service';
import { RegisterModel } from 'src/app/models/register.model';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {
  
  public Data = new RegisterModel;
  public DataProgramas: any;

  constructor(private backend: BackendService) { }

  ngOnInit() {
    this.GetPrograms();
  }


  GetPrograms () {
    this.backend.GetPrograms()
    .subscribe((resp: any) => {
      this.DataProgramas = resp;
    }, (error: any) => {
      console.log(error);
  });
  }

  SendForm() {
    this.backend.PostForm(this.Data)
    .subscribe((resp: any) => {
      console.log(resp); // No obtuve respuesta de este servicio, envié datos como parámetros y tambien en el body como json y la respuesta fue 200 Ok y null.
      Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Registro Exitoso',
        showConfirmButton: false,
        timer: 3500
      });
      this.Reset();
    }, (error: any) => {
      console.error(error);
    })
  }
  
  Reset() {
    this.Data.name = '';
    this.Data.comment = '';
    this.Data.program = '';
  }

}
