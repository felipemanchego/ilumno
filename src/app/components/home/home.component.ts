import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { BackendService } from 'src/app/services/backend.service';
import { RegisterModel } from 'src/app/models/register.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public DataNews: any;

  constructor(private backend: BackendService) { }
 
  ngOnInit() {
    this.GetNews();
  }


GetNews() {
  this.backend.GetNews().
  subscribe((resp: any) => {
    this.DataNews = resp;
  },(error: any) => {
    console.error(error);
  });
}



}