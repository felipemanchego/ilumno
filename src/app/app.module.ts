import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RegistroComponent } from './components/registro/registro.component';
import { RouterModule } from '@angular/router';
import { APP_ROUTING } from './app.routes';

@NgModule({
  imports: [
      BrowserModule,
      HttpClientModule,
      FormsModule,
      APP_ROUTING          
  ],
  declarations: [
      AppComponent
      ,
      HomeComponent,
      RegistroComponent         
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
