import { Component } from '@angular/core';
import { BackendService } from './services/backend.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ilumno';
  public year: number = Date.now();

  constructor() {

  }

}