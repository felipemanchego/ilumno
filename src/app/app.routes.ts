import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { RegistroComponent } from './components/registro/registro.component';



const APP_ROUTES: Routes = [

    { path: 'home', component: HomeComponent },
    { path: 'registro', component: RegistroComponent },
    { path: '**', pathMatch: 'full', redirectTo:  'home' },
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true, anchorScrolling: 'enabled'});
