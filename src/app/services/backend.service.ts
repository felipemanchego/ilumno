import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  urlApi = 'https://cms.qailumno.com/servicios';

  Headers: any;
  constructor(private http: HttpClient) { }

  GetNews() {
   return this.http.get( `${this.urlApi}/noticias` );
  }

  GetPrograms() {
    return this.http.get( `${this.urlApi}/programas` );
  }

  PostForm (body: any) {
    this.Headers = new HttpHeaders();
    this.Headers = this.Headers.set('Content-Type', 'application/json');
    const headers = this.Headers;
    const options = { params: body, headers };
    return this.http.post(`${this.urlApi}/registro`,body);
  }
}
